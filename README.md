# AutoMakefile

AutoMakefile made by Thomas HUGUES

# HOW TO USE IT ?

Put your auto_Makefile.sh program in your bin/ folder

Grant the access to the file with this command:
```bash
chmod 777 auto_Makefile.sh
```

Ready, Set, Go !

Just type
```bash
auto_Makefile.sh
```
in your terminal, and we're done.