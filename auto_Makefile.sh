#!/bin/bash
# auto_Makefile

touch Makefile

$NAME: "NAME"
$OBJ: "OBJ"
$FLAG: "FLAG"


# Header du Makefile (peut être retiré)

echo "##" > Makefile
echo "## POJECT NAME" >> Makefile
echo "## Makefile" >> Makefile
echo "## File description:" >> Makefile
echo "## Makefile done with auto_Makefile program" >> Makefile
echo "##" >> Makefile
echo "" >> Makefile

echo "" >> Makefile
echo "# How to use me ?" >> Makefile
echo "# Type make in your terminal to build your project" >> Makefile
echo "# Type make clean in your terminal to clean useless files" >> Makefile
echo "# Type make fclean in your terminal to clean useless files and erase executable file" >> Makefile
echo "# Type make re in your terminal to clean useless files, erase executable file, and rebuild your project" >> Makefile
echo "" >> Makefile
echo "" >> Makefile
echo "# DO NOT TRY TO MODIFY THE MAKEFILE IF YOU DON'T KNOW HOW TO DO" >> Makefile
echo "# IF YOU ADDED NEW FILES IN YOUR PROJECT, JUST RETYPE auto_Makefile.sh" >> Makefile
echo "" >> Makefile

# Récupération des fichiers .c dans tout les sous dossiers

FLIST=/tmp/.flist

# Pour compiler d'autres fichiers, il suffit de changer le '*.c' par '*.ext' (ext étant l'extension désirée, cpp, ...)

find -name '*.c' -not -path "*lib*" -not -path "*tests*" -not -path "*/.*" > $FLIST

FLIST=$(sort $FLIST)

for file in $FLIST;do
    if [[ $i -eq 0 ]];then
	echo -en "SRC\t=\t" >> Makefile
    else
	echo -en "\t\t" >> Makefile
    fi
    echo -e "$file\t\\" >> Makefile
    i=$(($i+1))
done

echo "" >> Makefile

echo  'OBJ	=	$(SRC:.c=.o) # Transforme tout les .c en .o pour compilation' >> Makefile

echo "" >> Makefile

echo "CFLAGS	=	-W -Wall -Werror -Wextra -g # Flags de compilation (peuvent être retirés (déconseillé))" >> Makefile

echo "" >> Makefile

echo "FLAG	=	-I./include # Dossier contenant les headers (.h)" >> Makefile

echo "" >> Makefile

read -p "Name of your project ? " name
echo "NAME	=	$name" >> Makefile

echo "" >> Makefile

echo 'all:	$(NAME)' >> Makefile

echo "" >> Makefile

echo '$(NAME):	$(OBJ)' >> Makefile

echo "" >> Makefile
echo "# Les @ servent à cacher les instructions executées, si vous voulez voir les étapes que fait le Makefile, retirez-les" >> Makefile
echo "" >> Makefile

echo '		@gcc -o $(NAME) $(OBJ) $(FLAG)' >> Makefile

echo "" >> Makefile

# Commande pour clean les fichiers inutiles

echo "clean:" >> Makefile

echo "		@rm -f src/*~" >> Makefile
echo "		@rm -f src/*.o" >> Makefile
echo "		@rm -f *.o" >> Makefile
echo "		@rm -f *~" >> Makefile
echo "		@rm -f vgcore*" >> Makefile
echo "		@rm -f *.gcno" >> Makefile
echo "		@rm -f *.gcda" >> Makefile
echo "		@rm -f include/*.gch" >> Makefile

echo "" >> Makefile

# Commande pour clean les fichiers inutiles ainsi que l'executable

echo "fclean:	clean" >> Makefile

echo '		@rm -f $(NAME)' >> Makefile

# Commande pour clean le dossier et build l'executable encore une fois

echo "" >> Makefile

echo "re:	fclean all" >> Makefile

echo "" >> Makefile

echo "# Sert à s'assurer qu'aucun fichier / dossier porte le nom des commandes à executer" >> Makefile
echo "" >> Makefile

echo ".PHONY:	all clean fclean re" >> Makefile

